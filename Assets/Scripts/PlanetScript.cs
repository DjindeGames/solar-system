﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetScript : MonoBehaviour {

	[SerializeField]
	bool isMoon = false;
	//The following value is only useful for moons and represents the planet they are rotating around
	[SerializeField]
	GameObject planet;

	//To customize the movement of the planet

	//Speed of the planet's rotation around the sun,
	//or speed of the moon around its planet (in degree's per frame)
	[SerializeField]
	float rotationSunSpeed = 30;
	//Speed of the planet/moon's self rotation (in degree's per frame)
	[SerializeField]
	float selfRotationSpeed = 5;

	//Directions of the rotations
	[SerializeField]
	bool revertRotationAroundSun = false;
	[SerializeField]
	bool revertSelfRotation = false;

	//If paused is set to true, the planet/moon is no longer moving or rotating
	bool paused = false;

	//The halo used when the planet/moon is selected
	Behaviour halo;

	// Use this for initialization
	void Start () {
		//Retrieves the halo component
		halo = (Behaviour) GetComponent ("Halo");
	}
	
	// Update is called once per frame
	void Update () {
		//If the planet/moon is frozen, nothing happens
		if (!paused) {
			if (isMoon) {
				//Reverts the rotation of the moon around the planet
				if (revertRotationAroundSun)
					//The moon rotates around the y axis of the planet to which it is attached
					transform.RotateAround (planet.transform.position, Vector3.up, -rotationSunSpeed * Time.deltaTime * SystemScript.getGlobalSpeed());
				else
					//The moon rotates around the y axis of the planet to which it is attached
					transform.RotateAround (planet.transform.position, Vector3.up, rotationSunSpeed * Time.deltaTime * SystemScript.getGlobalSpeed());
				//Reverts the self rotation of the moon
				if (revertSelfRotation)
					//The moon rotates around its y axis
					transform.Rotate (0, -selfRotationSpeed * SystemScript.getGlobalSpeed(), 0);
				else
					//The moon rotates around its y axis
					transform.Rotate (0, selfRotationSpeed * SystemScript.getGlobalSpeed(), 0);
			} else {
				//Reverts the rotation of the planet around the sun
				if (revertRotationAroundSun)
					//The planet rotates around the y axis of the origin of the scene which actually is the sun
					transform.RotateAround (Vector3.zero, Vector3.up, -rotationSunSpeed * Time.deltaTime * SystemScript.getGlobalSpeed());
				else
					//The planet rotates around the y axis of the origin of the scene which actually is the sun
					transform.RotateAround (Vector3.zero, Vector3.up, rotationSunSpeed * Time.deltaTime * SystemScript.getGlobalSpeed());
				//Reverts the self rotation of the planet
				if (revertSelfRotation)
					//The planet rotates around its y axis
					transform.Rotate (0, -selfRotationSpeed * SystemScript.getGlobalSpeed(), 0);
				else
					//The planet rotates around its y axis
					transform.Rotate (0, selfRotationSpeed * SystemScript.getGlobalSpeed(), 0);
			}
		}
	}

	//To select the planet
	void OnMouseOver(){
		if (Input.GetMouseButtonDown (0)) {
			//Unselects the currently selected planet if any
			SystemScript.unselectAll ();
			select ();
		}
	}

	//Unselects the planet
	public void unselect(){
		halo.enabled = false;
	}

	//Selects the planet
	void select(){
		halo.enabled = true;
		SystemScript.setPlanetName (gameObject.name);
		//Only planets orbits are drawn
		if (!isMoon)
			SystemScript.drawOrbit (Vector3.Distance(transform.position, new Vector3(0,0,0)));
	}

	//Pauses the planet
	public void pause(){
		paused = true;
	}

	//Resumes the planet's movements
	public void resume(){
		paused = false;
	}
}
