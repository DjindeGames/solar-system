﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {
	//The rotation speed used to move the camera around the sun
	[SerializeField]
	float speed = 1000;
	[SerializeField]
	float scrollSpeed = 5;

	//Minimum and maximum levels of zoom
	[SerializeField]
	float maxFOV = 140;
	[SerializeField]
	float minFOV = 20;

	//Our pivot point to rotate the camera
	GameObject sun;

	//The camera attached to the script
	Camera cam;

	//The horizontal axis the camera is rotating around
	Vector2 axis;

	// Use this for initialization
	void Start () {
		//Retrieves gameobjects and components
		sun = GameObject.Find ("sun");
		cam = GetComponent<Camera> ();
		//Initialises camera's rotation
		transform.LookAt (sun.transform);
	}
	
	// Update is called once per frame
	void Update () {
		//To rotate the camera around the sun by dragging mouse
		if (Input.GetMouseButton (0)) {
			//Gets the vector that is perpendicular to the camera's position (in 2D) and crosses the origin of the scene, eg the sun
			axis = Vector2.Perpendicular (new Vector2 (transform.position.x, transform.position.z)).normalized;
			//Vertical rotation around "axis" so that it is independant of the camera's position around the sun
			//We could have used either axis X or Z but the rotation is far more practical that way
			//To avoid vertical rotation's blocking on poles (conflict on mouse dragging directions
			if (axis.x < 0) {
				transform.RotateAround (Vector3.zero, new Vector3 (axis.x, 0, axis.y), Input.GetAxis ("Mouse Y") * speed * Time.deltaTime);
			} else {
				transform.RotateAround (Vector3.zero, new Vector3 (axis.x, 0, axis.y), -Input.GetAxis ("Mouse Y") * speed * Time.deltaTime);
			}
			//Horizontal rotation is always around the Y axis and is independant of the relative camera's position around the sun
			transform.RotateAround (Vector3.zero, new Vector3 (0, 1, 0), Input.GetAxis ("Mouse X") * speed * Time.deltaTime);
		}
		//To unzoom (increases camera's FOV)
		if (Input.GetAxis ("Mouse ScrollWheel") < 0 && cam.fieldOfView < maxFOV) {
			cam.fieldOfView += scrollSpeed;
		}
		//To zoom (decreases camera's FOV)
		if (Input.GetAxis ("Mouse ScrollWheel") > 0 && cam.fieldOfView > minFOV) {
			cam.fieldOfView -= scrollSpeed;
		}
	}
}
