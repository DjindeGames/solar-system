﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SystemScript : MonoBehaviour {
	//The following is the global system's speed multipliers, all planets are actually using it in the calculation
	//Of their self rotation and their rotation around the sun
	[SerializeField]
	static float globalSpeed = 1f;
	[SerializeField]
	static float minSpeed = 0.1f;
	[SerializeField]
	static float maxSpeed = 20f;

	//To pause the system
	static bool paused = false;

	//The following are UI elements
	static Text planetName;
	static GameObject menu;
	//To toggle the menu using the menu button
	static bool menuActive = false;
	//A slider that displays the current system speed
	static Slider speedSlider;

	//The number of segments used to draw the orbits
	static int segments = 100;
	//Used to draw the orbits
	static LineRenderer orbitRenderer;
	//Our planets
	static GameObject[] planets;

	// Use this for initialization
	void Start () {
		//Retrieving gameobjects
		planetName = GameObject.Find ("planetName").GetComponent<Text> ();
		orbitRenderer = GameObject.Find("sun").GetComponent<LineRenderer> ();
		speedSlider = GameObject.Find ("speedSlider").GetComponent<Slider> ();
		menu = GameObject.Find ("menu");
		//We are using the scale to hide the menu because setActive somehow causes huge lag on the first time the menu is displayed
		//That way the menu is loaded at the same time with the scene
		menu.transform.localScale = new Vector3 (0, 0, 0);
		planets = GameObject.FindGameObjectsWithTag ("Planet");
		//Initializing values
		planetName.text = "";
		speedSlider.maxValue = maxSpeed;
		speedSlider.minValue = minSpeed;
		speedSlider.value = globalSpeed;
	}
	
	// Update is called once per frame
	void Update () {
		//Unselect all planets on a right click
		if (Input.GetMouseButtonDown (1))
			unselectAll ();
		//Pauses or resumes the system on pressing space key
		if (Input.GetKeyDown (KeyCode.Space)) {
			if (paused)
				resume ();
			else
				pause ();
		}
		//Increases the global speed multiplier on pressing the plus key
		if (Input.GetKey (KeyCode.KeypadPlus)) {
			increaseSpeed ();
		}
		//Decreases the global speed multiplier on pressing the minus key	
		if (Input.GetKey (KeyCode.KeypadMinus)) {
			decreaseSpeed ();
		}
	}

	//Used by the planets to calculate their rotations speed
	public static float getGlobalSpeed(){
		return globalSpeed;
	}

	//To update the name of the selected planet
	public static void setPlanetName(string name){
		planetName.text = name;
	}

	//Unselects all planets
	public static void unselectAll(){
		foreach (GameObject planet in planets) {
			planet.GetComponent<PlanetScript> ().unselect ();
		}
		//Erases the selected planet's name
		planetName.text = "";
		//Erases the drawn orbit (deletes all points that compose the circle
		orbitRenderer.positionCount = 0;
	}

	//Pauses the system, eg all planets are individually paused
	public static void pause(){
		paused = true;
		foreach (GameObject planet in planets) {
			planet.GetComponent<PlanetScript> ().pause ();
		}
	}

	//Resumes the system's movements, eg all planets' movements are individually resumed
	public static void resume(){
		paused = false;
		foreach (GameObject planet in planets) {
			planet.GetComponent<PlanetScript> ().resume ();
		}
	}

	//Increases the global speed multiplier
	static void increaseSpeed(){
		if (globalSpeed < maxSpeed)
			globalSpeed += 0.1f;
		speedSlider.value = globalSpeed;
	}

	//Decreases the global speed multiplier
	static void decreaseSpeed(){
		//The + 0.1f is used to avoid going under minSpeed
		//(weird glitch)
		if (globalSpeed >= minSpeed + 0.1f)
			globalSpeed -= 0.1f;
		speedSlider.value = globalSpeed;
	}

	//To toggle the menu by clicking the menu button
	//Using scale to avoid the lag on first click
	public void toggleMenu(){
		if (!menuActive) {
			menuActive = true;
			menu.transform.localScale = new Vector3 (1, 1, 1);
		} else {
			menuActive = false;
			menu.transform.localScale = new Vector3 (0, 0, 0);
		}
	}
		
	//To draw the orbit of the selected planet
	public static void drawOrbit(float radius){
		float x;
		float z;
		float angle = 0f;

		//The "resolution" of our circle
		orbitRenderer.positionCount = segments + 1;

		//Calculates the coordinates of each point that composes the circle and adds it to the line
		for (int i = 0; i < (segments + 1); i++)
		{
			x = Mathf.Sin (Mathf.Deg2Rad * angle) * radius;
			z = Mathf.Cos (Mathf.Deg2Rad * angle) * radius;
			//Adds the point to the circle
			orbitRenderer.SetPosition (i,new Vector3(x,0,z) );
			//Increases the angle for the next point coordinates calculation
			angle += (360f / segments);
		}
	}

	//Exits the app
	public void exit(){
		Application.Quit ();
	}
}
