﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

/*********************************************************************/
/****DO NOT FORGET TO ENABLE THE TESTXIWEN GAMEOBJECT IN THE SCENE****/
/*********************************************************************/

public class TestXiwen : MonoBehaviour {
	//The size wanted for the test arrays
	[SerializeField]
	int arraySize;
	//The size wanted for the test arrays
	[SerializeField]
	int range;
	//The number of tests wanted
	[SerializeField]
	int numberOfTests;
	//The unsorted array
	int[] myArrayU;
	//The sorted array
	int[] myArrayS;
	//To calculate the average execution time
	int[] mergeSortTimes;
	int[] quickSortTimes;
	//To know how much time it took to sort the array
	int time;

	// Use this for initialization
	void Start () {
		//Initialising the random int generator using system clock
		UnityEngine.Random.InitState ((int)System.DateTime.Now.Ticks);

		//Initializing arrays
		myArrayU = new int[arraySize];
		myArrayS = new int[arraySize];
		mergeSortTimes = new int[numberOfTests];
		quickSortTimes = new int[numberOfTests];

		//Performing tests
		//We are comparing the speeds of our sorting method and the Array.Sort one
		//Results show that Array.Sort is about 4 times faster than our sort method (depending on the size of the array)
		//Array.Sort actually implements the QuickSort algorithm whereas ours uses the MergeSort algorithm
		for (int i = 0; i < numberOfTests; i++) {
			Debug.Log ("Test n° " + (i + 1) + ":");
			//Fills the aray with random values and displays it
			fillArray (myArrayU);
			debugArray (myArrayU);

			/***MERGESORT***/

			//Starts the "timer"
			//To have a precise duration is ms without using a timer
			time = ((System.DateTime.Now.Minute * 1000 * 60) + (System.DateTime.Now.Second * 1000) +  System.DateTime.Now.Millisecond);
			myArrayS = sort(myArrayU);
			//Stores the execution time
			mergeSortTimes[i] = ((System.DateTime.Now.Minute * 1000 * 60) + (System.DateTime.Now.Second * 1000) +  System.DateTime.Now.Millisecond) - time;
			//Displays the sorted array
			debugArray (myArrayS);

			/***QUICKSORT***/

			//Starts again our timer
			//To have a precise duration is ms without using a timer
			time = ((System.DateTime.Now.Minute * 1000 * 60) + (System.DateTime.Now.Second * 1000) +  System.DateTime.Now.Millisecond);
			Array.Sort (myArrayU);
			//Stores the execution time
			quickSortTimes[i] = ((System.DateTime.Now.Minute * 1000 * 60) + (System.DateTime.Now.Second * 1000) +  System.DateTime.Now.Millisecond) - time;
		}
		Debug.Log ("Average execution time for MergeSort :" + average (mergeSortTimes) + "ms.");
		Debug.Log ("Average execution time for QuickSort :" + average (quickSortTimes) + "ms.");
	}
		
	//Sorts an array using the generic MergeSort algorithm
	//Array.sort actually uses the QuickSort algorithm which is generally more efficient for sorting arrays but has a
	//greater worst case complexity than MergeSort, MergeSort requires more memory than QuickSort.
	//https://www.geeksforgeeks.org/quick-sort-vs-merge-sort/
	//After some researches, it seems that MergeSort is better at sorting Lists, whereas QuickSort is better at sorting Arrays
	//I chose MergeSort anyways since it is the one we have been talking the most about in courses
	int[] sort(int[] tab){
		//Left and right part of the array to sort
		int[] left;
		int[] right;
		//If the array contains only 1 element, it is already sorted
		if (tab.Length <= 1)
			return tab;
		else {
			//Cutting the array in half
			int i = tab.Length / 2;
			//Left becomes the first part of the array to sort
			left = new int[i];
			//Right becomes the second part of the array to sort
			right = new int[tab.Length - i];
			Array.Copy (tab, 0, left, 0, i);
			Array.Copy (tab, i, right, 0, tab.Length - i);
			//Recursive strategy (divide and conquer)
			return merge (sort (left), sort (right));
		}
	}

	//Merges two sorted arrays
	int[] merge(int[] left, int [] right){
		int[] merged = new int[right.Length + left.Length];
		//The current indexes in both left and right arrays
		int iL = 0, iR = 0;
		for (int iM = 0 ; iM < merged.Length ; iM++){
			//If we have already have copied the whole right array
			if (iR == right.Length) {
				//Then we just have to copy the remaining elements in the left array
				merged [iM] = left [iL];
				iL++;
			} else {
				//If we have already have copied the whole left array
				if (iL == left.Length) {
					//Then we just have to copy the remaining elements in the right array
					merged [iM] = right [iR];
					iR++;
				} else {
					//We choose the littlest element by comparing left[iL] and right[iR]
					if (left [iL] <= right [iR]) {
						merged [iM] = left [iL];
						iL++;
					} else {
						merged [iM] = right [iR];
						iR++;
					}
				}
			}
		}
		return merged;
	}

	//To display an array
	void debugArray(int[] array){
		string content = "BEGIN | ";
		for (int i = 0 ; i < array.Length ; i++) {
			content += array [i] + " | ";
		}
		content += "END";
		Debug.Log (content);
	}

	//Fills the given array with random ints included in [-range,+range]
	void fillArray(int[] array){
		for (int i = 0; i < array.Length; i++) {
			array [i] = UnityEngine.Random.Range (-range, range);
		}
	}


	//Calculates the average value of an array
	float average (int[] tab){
		int sum = 0;
		for (int i = 0; i < tab.Length; i++) {
			sum += tab [i];
		}
		return ((float)sum / (float)numberOfTests);
	}
}
